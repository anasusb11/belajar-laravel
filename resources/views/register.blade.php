<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register | Sanberbook</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
    <label>First Name:</label> <br> <br>
    <input type="text" name="first_name"> <br><br>
    <label>Last Name:</label> <br> <br>
    <input type="text" name="last_name"> <br><br>
    <label>Gender:</label> <br> <br>
    <input type="radio" name="gender">Male <br>
    <input type="radio" name="gender">Female <br>
    <input type="radio" name="gender">Other <br><br>
    <label>Nationality:</label> <br> <br>
    <select name="nation" id="">
        <option value="1">Indonesian</option>
        <option value="2">Singapore</option>
        <option value="3">Australian</option>
    </select> <br><br>
    <label>Language Spoken:</label> <br> <br>
    <input type="checkbox">Bahasa Indonesia <br>
    <input type="checkbox">English <br>
    <input type="checkbox">Other<br><br>

    <label>Bio:</label> <br> <br>
    <textarea name="alamat" id="" cols="30" rows="3"></textarea>
    <br><br>
    <button type="submit">Sign Up</button>
    </form>




</body>
</html>
